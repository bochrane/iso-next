import firebase from 'firebase/app';
import 'firebase/auth';
import { firebaseConfig } from '../../config';

const valid =
  firebaseConfig && firebaseConfig.apiKey && firebaseConfig.projectId;

try {
  firebase.initializeApp(firebaseConfig);
} catch (err) { console.error('Firebase init error: ', err) }
const firebaseAuth = firebase.auth;
class FirebaseHelper {
  isValid = valid;
  EMAIL = 'email';
  FACEBOOK = 'facebook';
  GOOGLE = 'google';
  GITHUB = 'github';
  TWITTER = 'twitter';

  login = (provider, info) => {

    console.log('INFO IN IS', info);

    switch (provider) {
      case this.EMAIL:
        return firebaseAuth().signInWithEmailAndPassword(
          info.email,
          info.password
        );
      case this.FACEBOOK:
        return firebaseAuth().FacebookAuthProvider();
      case this.GOOGLE:
        return firebaseAuth().GoogleAuthProvider();
      case this.GITHUB:
        return firebaseAuth().GithubAuthProvider();
      case this.TWITTER:
        return firebaseAuth().TwitterAuthProvider();
      default:
    }
  };

  logout = () => {
    return firebaseAuth().signOut();
  };

  isAuthenticated = () => {
    firebaseAuth().onAuthStateChanged(user => !!user);
  };

  resetPassword(email) {
    return firebaseAuth().sendPasswordResetEmail(email);
  }
}

export default new FirebaseHelper();
