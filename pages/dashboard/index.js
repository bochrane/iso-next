import Page from '../../hocs/securedPage';
import App from '../../containers/App';

export default Page(() => <App />);
