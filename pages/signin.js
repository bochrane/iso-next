import Page from '../hocs/defaultPage';
import Signin from '../containers/Page/signin';

export default Page(() => <Signin />);
