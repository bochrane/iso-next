import Page from '../hocs/defaultPage';
import CreateTeam from '../containers/Page/createteam';

export default Page(() => <CreateTeam />);
